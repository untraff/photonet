# PhotoNet - Small Social Net For Photographers

## Примеры использования:
Получение всех фото сущности, например, получить все одобренные фото столов **(Thing.pk=1)**

```
from .models import Thing
table = Country.objects.get(pk=1)
c.photos.approved_for_instance()
```
----
Тот же пример, но для не одобренных фото:
```
from .models import Thing
table = Thing.objects.get(pk=1)
table.photos.denied_for_instance()
```
----
Получить все фото типа, например: получить все фото из Албании - и самой Албании,  и столов, и стульев из неё
```
from .models import Country
albany = Country.objects.get(pk=1)
albany.photos.approved()
```
----
Получить не одобренные фото, например, чтобы вывести список для модераторов.
Можно обратиться как наряпмую к модели...:

```
from .models import Photo
all_denied_photos = Photo.objects.denied()
```
так и к конкреному типу:
```
from .models import Country
russia = Country.objects.get(pk=2)
russia.photos.denied()
```


