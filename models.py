from django.contrib.auth.base_user import AbstractBaseUser
from django.db import models
from django.db.models import FileField, QuerySet
from django.contrib.auth.models import User as _User


def get_upload_path(instance, filename):
    #  TODO: реализовать
    return 'static/'


class BaseManager(models.Manager):  # ApprovedPhotosMixin):
    def with_related(self):
        return self.prefetch_related('photos')


class PhotoManager(models.Manager):

    def approved(self) -> QuerySet:
        """Возвращает одобренные фотографии для типа.
        """
        return self.filter(approved=True)

    def denied(self) -> QuerySet:
        """Возвращает отклоненные фотографии для типа.
        """
        return self.filter(approved=False)

    def approved_for_instance(self) -> QuerySet:
        """Возвращает фото для определенного объекта
        """
        qs = self.approved()
        return self._get_filtered_qs(qs, self.instance)

    def denied_for_instance(self) -> QuerySet:
        qs = self.denied()
        return self._get_filtered_qs(qs, self.instance)

    def _get_filtered_qs(self, qs, instance) -> QuerySet:
        # TODO: придумать более элегантное решение
        if isinstance(instance, Country):
            return qs.filter(country_id=instance.pk, city__isnull=True, thing__isnull=True)
        elif isinstance(instance, City):
            return qs.filter(country=instance.country, city__id=instance.pk, thing__isnull=True)
        elif isinstance(instance, Thing):
            return qs.filter(country=instance.city.country, city=instance.city, thing__id=instance.pk)
        return qs


class FileAbstract(models.Model):
    """Abstract class for all files in project
    """
    file = FileField('Файл', upload_to=get_upload_path, db_index=True)
    original_name = models.CharField('Оригинальное название', max_length=500)

    class Meta:
        ordering = ['id']
        abstract = True


class Photo(FileAbstract):
    # TODO: сделать валидаторы для thing, country, city
    # Например чтобы не выбрать Страна: Россиия, Город: New York
    # TODO: сделать общий валидатор, чтобы одно из полей, кроме юзера, было не пустое

    is_public = models.BooleanField('Опубликовано', default=True)
    approved = models.BooleanField('Одобрено', default=False)

    user = models.ForeignKey('User', related_name='users', on_delete=models.CASCADE)
    country = models.ForeignKey('Country', related_name='photos', on_delete=models.CASCADE, null=True, blank=True)
    city = models.ForeignKey('City', related_name='photos', on_delete=models.CASCADE, null=True, blank=True)
    thing = models.ForeignKey('Thing', related_name='photos', on_delete=models.CASCADE, null=True, blank=True)

    objects = PhotoManager()

    def __str__(self):
        return f'{self.country}/{self.city}/{self.thing}/{self.original_name}'


class Country(models.Model):
    name = models.CharField(max_length=255)

    objects = BaseManager()

    def __str__(self):
        return self.name


class City(models.Model):
    name = models.CharField(max_length=255)
    country = models.ForeignKey('Country', related_name='cities', on_delete=models.CASCADE)

    objects = BaseManager()

    def __str__(self):
        return self.name

    class Meta:
        ordering = ['country', 'name']


class Thing(models.Model):
    name = models.CharField(max_length=255)
    city = models.ForeignKey('City', related_name='things', on_delete=models.CASCADE)

    objects = BaseManager()

    def __str__(self):
        return self.name

    class Meta:
        ordering = ['city', 'name']


class User(_User):
    # TODO: понять условие задания :)
    thing = models.ForeignKey('Thing', related_name='users', on_delete=models.CASCADE)

    objects = BaseManager()
